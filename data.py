import random
import time
from text import *
from telebot import types


def start_story():
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton(text='Сюжет', callback_data='start_story'))
    return markup


def one_of_final():
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton(text='Пройти еще раз', callback_data='start_story'))
    markup.add(types.InlineKeyboardButton(text='Статистика', callback_data='statistic'))
    markup.add(types.InlineKeyboardButton(text='Вернуться назад', callback_data='none'))
    return markup


def script_ways(text1, call1, text2, call2):
    markup = types.InlineKeyboardMarkup()
    button1 = types.InlineKeyboardButton(text=text1, callback_data=call1)
    button2 = types.InlineKeyboardButton(text=text2, callback_data=call2)
    markup.add(button1)
    markup.add(button2)
    return markup


def markup_games():
    markup_inline = types.InlineKeyboardMarkup()
    game_1 = types.InlineKeyboardButton(text=story_mode_title, callback_data='start_story')
    game_2 = types.InlineKeyboardButton(text=r_p_s_game_title, callback_data='rock_paper_scissors')
    back = types.InlineKeyboardButton(text='Описание', callback_data='description')
    markup_inline.add(game_1, game_2)
    markup_inline.add(back)
    return markup_inline


def markup_choice_game():
    markup_reply = types.InlineKeyboardMarkup()
    inline_temp = []
    for i in range(3):
        for j in range(3):
            word = str(i + 1) + str(j + 1)
            inline_temp.append(types.InlineKeyboardButton(text='\U0001F49B', callback_data=word))
        markup_reply.add(inline_temp[0], inline_temp[1], inline_temp[2])
        inline_temp.clear()
    return markup_reply


def markup_r_p_s_game():
    markup_keys = types.InlineKeyboardMarkup()
    rock = types.InlineKeyboardButton(text='✊', callback_data=r_p_s_keys[0])
    paper = types.InlineKeyboardButton(text='\U0001F44B', callback_data=r_p_s_keys[1])
    scissors = types.InlineKeyboardButton(text='✌', callback_data=r_p_s_keys[2])
    markup_keys.add(rock, paper, scissors)
    return markup_keys


def final_checking(a: int, b: bool):
    if b:
        return 'Концовка номер ' + str(a) + ' - ' + '\U00002705 \n'
    else:
        return 'Концовка номер ' + str(a) + ' - ' + '\U0000274C \n'


def generate_prize():
    qwerty = 'ABCDEFGHIJ1234567'
    code = ''
    for i in range(7):
        code += random.choice(qwerty)
    return '\n     <b>' + code + prize_text


def text_input(user):
    time_str = ''
    user.played_counter += 1

    if user.played_counter > 3:
        user.out_of_counter = True

    if user.out_of_counter:
        if not user.timer_started:
            user.start_time_stamp = time.time()
            user.timer_started = True
        else:
            if time.time() - user.start_time_stamp > 60:
                user.start_time_stamp = None
                user.timer_started = False
                user.played_counter = 0
            else:
                time_str += wait_message + str(int(
                    float(60) - (time.time() - user.start_time_stamp))) + ' ceк.'
    return time_str


def r_p_s_string(unit, item):
    rock, paper, scissors = r_p_s_keys[0], r_p_s_keys[1], r_p_s_keys[2]
    win = (unit == rock and item == scissors) or \
          (unit == paper and item == rock) or \
          (unit == scissors and item == paper)
    lose = (item == rock and unit == scissors) or \
           (item == paper and unit == rock) or \
           (item == scissors and unit == paper)
    draw = (unit == rock and item == rock) or \
           (unit == paper and item == paper) or \
           (unit == scissors and item == scissors)
    if win:
        return [unit + ' - ' + item + '\n' + you_win, True]
    elif lose:
        return [unit + ' - ' + item + '\n' + you_lose, False]
    elif draw:
        return [unit + ' - ' + item + '\n' + draw_fight, False]



