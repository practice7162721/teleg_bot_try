import telebot
import random
from telebot import types
from user import *
from data import *
from text import *
token = '6119110299:AAGKuyKXNr0EknmVvO4D4nduLM837xYqgSY'
bot = telebot.TeleBot(token)
client = User()


@bot.message_handler(commands=['start'])
def start(message):
    client.user_id = message.from_user.id
    client.first_name = message.from_user.first_name
    last_name = message.from_user.last_name
    if last_name is None:
        last_name = ''
    mess = f' <em>Приветствую тебя <b>{client.first_name} {last_name}</b> \U0001F63A \n' + start_text + '</em>'
    bot.send_message(message.chat.id, mess, parse_mode='html', reply_markup=markup_games())


@bot.message_handler(commands=['finals'])
def finals(message):
    message_of_final = ''
    for i in client.finals:
        message_of_final += final_checking(i[0], i[1])
    bot.send_message(message.chat.id, message_of_final)


@bot.callback_query_handler(func=lambda call: True)
def game(call):
    chat_id = call.message.chat.id
    if call.data == 'choice':
        bot.send_message(call.message.chat.id, story_mode_title, reply_markup=markup_choice_game())
    elif call.data == 'rock_paper_scissors':
        bot.send_message(call.message.chat.id, r_p_s_game_title + ' ?', reply_markup=markup_r_p_s_game())
    elif call.data == 'description':
        bot.send_message(call.message.chat.id, description, parse_mode='html', reply_markup=markup_games())
    elif call.data == 'start_story':
        texts = ['Да. О какой сумме идет речь?', 'get_money', 'Нет. Я наверное пойду', 'way_to_robbery']
        bot.send_photo(chat_id, open('../teleg_bot_try/photos/bank_employee_start.png', 'rb'))
        bot.send_message(chat_id, story_script_start, reply_markup=script_ways(texts[0], texts[1], texts[2], texts[3]))
    elif call.data == 'way_to_robbery':
        bot.send_message(chat_id, robbery_start_text)
        time.sleep(1)
        texts = ['Выключить телефон', 'off_phone', 'Просто положить', 'dont_touch_phone']
        bot.send_photo(chat_id, open('../teleg_bot_try/photos/bank_robbers.png', 'rb'))
        bot.send_message(chat_id, turn_off_your_phone, reply_markup=script_ways(texts[0], texts[1], texts[2], texts[3]))
    elif call.data == 'get_money':
        texts = ['Еду на такси', 'go_go_taxi', 'Пройдусь пешком', 'out_walk']
        bot.send_photo(chat_id, open('../teleg_bot_try/photos/taxi_or_walk.png', 'rb'))
        bot.send_message(chat_id, get_money_text, reply_markup=script_ways(texts[0], texts[1], texts[2], texts[3]))
    elif call.data == 'go_go_taxi':
        bot.send_photo(chat_id, open('../teleg_bot_try/photos/watching_tv.png', 'rb'))
        bot.send_message(chat_id, taxi_final, reply_markup=one_of_final())
        client.finals[0][1] = True
    elif call.data == 'out_walk':
        texts = ['Подойти к ним', 'go_to_band', 'Пройти мимо', 'dont_go_to_band']
        bot.send_photo(chat_id, open('../teleg_bot_try/photos/street_band_asks.png', 'rb'))
        bot.send_message(chat_id, street_ask, reply_markup=script_ways(texts[0], texts[1], texts[2], texts[3]))
        time.sleep(3)
    elif call.data == 'off_phone':
        client.phone = False
        texts = ['Взять пистолет', 'take_gun', 'Не трогать пистолет', 'dont_take_gun' ]
        bot.send_photo(chat_id, open('../teleg_bot_try/photos/gun_in_table.png', 'rb'))
        bot.send_message(chat_id, attention_gun, reply_markup=script_ways(texts[0], texts[1], texts[2], texts[3]))
    elif call.data == 'dont_touch_phone':
        client.phone = True
        bot.send_photo(chat_id, open('../teleg_bot_try/photos/gun_in_table.png', 'rb'))
        markup = script_ways('Взять пистолет', 'take_gun', 'Не трогать пистолет', 'dont_take_gun')
        bot.send_message(chat_id, attention_gun, reply_markup=markup)
    elif call.data == 'take_gun':
        if client.phone:
            bot.send_photo(chat_id, open('../teleg_bot_try/photos/broken.png', 'rb'))
            bot.send_message(chat_id, your_resistance_with_on_phone, reply_markup=one_of_final())
            client.finals[1][1] = True
        else:
            bot.send_photo(chat_id, open('../teleg_bot_try/photos/tricky final.png', 'rb'))
            bot.send_message(chat_id, your_resistance_with_off_phone, reply_markup=one_of_final())
            client.finals[2][1] = True
    elif call.data == 'dont_take_gun':
        if client.phone:
            bot.send_photo(chat_id, open('../teleg_bot_try/photos/broken.png', 'rb'))
            bot.send_message(chat_id, without_gun_on_phone, reply_markup=one_of_final())
            client.finals[4][1] = True
        else:
            bot.send_photo(chat_id, open('../teleg_bot_try/photos/watching_tv.png', 'rb'))
            bot.send_message(chat_id, without_gun_off_phone, reply_markup=one_of_final())
            client.finals[3][1] = True
    elif call.data == 'go_to_band':
        client.answer = True
        texts = ['Отдать деньги', 'send_money', 'Оказать сопративление', 'fight_band']
        bot.send_photo(chat_id, open('../teleg_bot_try/photos/give_your_money.png', 'rb'))
        bot.send_message(chat_id, take_your_money, reply_markup=script_ways(texts[0], texts[1], texts[2], texts[3]))
    elif call.data == 'dont_go_to_band':
        client.answer = False
        texts = ['Отдать деньги', 'send_money', 'Оказать сопративление', 'fight_band']
        bot.send_photo(chat_id, open('../teleg_bot_try/photos/boxing_gloves.png', 'rb'))
        bot.send_message(chat_id, band_came_up, reply_markup=script_ways(texts[0], texts[1], texts[2], texts[3]))
    elif call.data == 'fight_band':
        if client.answer:
            bot.send_photo(chat_id, open('../teleg_bot_try/photos/norris.png', 'rb'))
            bot.send_message(chat_id, fight_final_1, reply_markup=one_of_final())
            client.finals[7][1] = True
        else:
            bot.send_photo(chat_id, open('../teleg_bot_try/photos/broken.png', 'rb'))
            bot.send_message(chat_id, fight_final_2, reply_markup=one_of_final())
            client.finals[5][1] = True
    elif call.data == 'send_money':
        bot.send_photo(chat_id, open('../teleg_bot_try/photos/tricky final.png', 'rb'))
        bot.send_message(chat_id, money_for_band, reply_markup=one_of_final())
        client.finals[6][1] = True
    elif call.data == 'statistic':
        message_of_final = ''
        for i in client.finals:
            message_of_final += final_checking(i[0], i[1])
        bot.send_message(chat_id, message_of_final, reply_markup=start_story())
    elif choice_keys.count(call.data) > 0:
        time_str = text_input(client)
        prize = ''
        if time_str == '':
            prize = generate_prize()
        win = random.choice(choice_keys)
        if call.data == win:
            bot.send_message(call.message.chat.id, you_win + prize +
                             time_str, parse_mode='html', reply_markup=markup_games())
        else:
            bot.send_message(call.message.chat.id, you_lose +
                             time_str, reply_markup=markup_games())
    elif r_p_s_keys.count(call.data) > 0:
        time_str = text_input(client)
        prize = ''
        if time_str == '':
            prize = generate_prize()
        enemy = random.choice(r_p_s_keys)
        if r_p_s_string(call.data, enemy)[1]:
            bot.send_message(call.message.chat.id, '(' + client.first_name + ') - (Бот) \n'
                             + r_p_s_string(call.data, enemy)[0] + prize + time_str
                             , parse_mode='html', reply_markup=markup_games())
        else:
            bot.send_message(call.message.chat.id, '(' + client.first_name + ') - (Бот) \n'
                             + r_p_s_string(call.data, enemy)[0] + time_str
                             , parse_mode='html', reply_markup=markup_games())
    else:
        bot.send_message(call.message.chat.id, default_text, parse_mode='html', reply_markup=markup_games())


@bot.message_handler()
def default_message(message):
    bot.send_message(message.chat.id, default_text, parse_mode='html', reply_markup=markup_games())


bot.polling(none_stop=True)