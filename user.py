class User:
    def __init__(self):
        self.user_id = 0
        self.played_counter = 0
        self.first_name = 'Ты'

        self.start_time_stamp = None
        self.now_time_stamp = None
        self.time_offset = None

        self.timer_started = False
        self.out_of_counter = False
        self.time_out = False

        self.phone = False
        self.answer = False

        self.finals = [
            [1, False],
            [2, False],
            [3, False],
            [4, False],
            [5, False],
            [6, False],
            [7, False],
            [8, False]
        ]

